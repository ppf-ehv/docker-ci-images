# INIT

## Images

### base
Base Image

### java
Java 8 Image

Based on `base`

### node
Node 5.6.0 Image

Based on `java`

### web-test
Headless Selenium with Chrome Image

Based on `node`

### android
Android SDK version 24.4.1
Cordova 6.0.0.

Based on `node`

### sass
Ruby 2.2
Compass 1.0.3

Based on `web-test`

### dind-java
Dind
Java 8

Based on gitlab/dind:latest

### dind-java-awscli
Java 8
AWS client utilities

Based on dind-java:latest
